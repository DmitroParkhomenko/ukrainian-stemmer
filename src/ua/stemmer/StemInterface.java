package ua.stemmer;

/**
 * Created by dmitro on 26.02.2015.
 */
public interface StemInterface {
    String stem(String input);
}
