package ua.stemmer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dmitro on 24.02.2015.
 */
public class RulesStem implements StemInterface {
    private final String UKRAINIAN_ALFABET = "[абвгґдеєжзиіїйклмнопрстуфхцчшщьюя]";
    private final String VOWELS = "[аеиіоуєюяї]";

    private final String ADJECTIVE_AND_PARTICIPLE_COMPLEX_SUFFIX =
            "(жн|нн|шн|жн|ан|ен|ин|їн|єн|ян|езн|лен|льн|анн|енн|(и|і|ї)чн|" +
                    "іш|вш|" +
                    "ов|ев|єв|ів|їв|лив|" +
                    "уч|юч|ач|яч|жч|" +
                    "ек|ськ|цьк|(е|есе|ісі|юсі)ньк|" +
                    "ат|уват|юват|овит|аст|яст|" +
                    "ол)";

    private final String ADJECTIVE_COMPLEX_END =
            "(ну|ь?ого|ь?ою|ь?ої|[ийі]?ми|ову|ій|ий|ова|ове|ів|їй|єє|еє|ем|им|их|іх|ому|ше|ашні|шне|шна|ячі|[аеіуєюяї])$";
    private final String ADJECTIVE_END_EXCEPTIONS = ".*(ння)$";
    private final String ADJECTIVE_SIMPlE_SUFFIX_WITH_END = UKRAINIAN_ALFABET + "(ну|ий|ийі|ова|ову|ове|ові|ому|ячі|ин|ен|ке)$";

    private final String PARTICIPLE_COMPLEX_END = "(ий|ого|ому|им|ім|ою|уч|юч|ій|их|йми|их)$";

    private final String VERB_COMPLEX_SUFFIX = "(ува|юва|ну|ит|йде|йду)";
    private final String VERB_SIMPLE_SUFFIX = "[иіш]";
    private final String VERB_COMPLEX_END =
            "(сь|ся|ив|[аяуюиі]?ть|ав|ли|учи|ячи|в?ши|ше|ме|[аяі]?ти|но|то|ла|ло|юю|джу|имо|и?те|йду|иму|диш|ять|ов)$";
    private final String VERB_SIMPLE_END = "[уюєш]$";

    private final String VERB_ONLY_END = "([аяуюиі]?ть|[аяі]?ти)$";

    private final String NOUN_COMPLEX_SUFFIX =
            "он|очок|ечок|очи|ечк|иц|ик|ищ|ар|яр|ник|льник|іст|ист|енн|ізм|изм|ств|ач|иця|оньк|еньк|ець|єць|ість|" +
                    "тель|нн|ин";
    private final String NOUN_SIMPLE_SUFFIX = "[хке]";
    private final String NOUN_COMPLEX_END =
            "(ев|ов|[яа]ми|еи|ей|ой|ий|иям|ям|ием|ем|ам|ом|ах|иях|ях|ию|ью|ия|ья|ові|еї|ею|єю|ою|еві|ем|єм|ів|їв)$";
    private final String NOUN_SIMPLE_END = "[аеийоуыьюяіїєю]$";

    private final String ADVERB_SUFFIX = "(ечк|еньк|ісіньк|есеньк|ш|іш|н?н)";
    private final String ADVERB_END = "(о|е|ому|и|чі)$";

    private final Pattern ADJECTIVE_COMPLEX_SUFFIX_WITH_END_P;
    private final Pattern ADJECTIVE_SIMPlE_SUFFIX_WITH_END_P;
    private final Pattern PARTICIPLE_COMPLEX_SUFFIX_WITH_END_P;
    private final Pattern VERB_COMPLEX_SUFFIX_WITH_END_P;
    private final Pattern VERB_ONLY_END_P;
    private final Pattern VERB_COMPLEX_SUFFIX_WITH_SIMPLE_END_P;
    private final Pattern VERB_SIMPLE_SUFFIX_WITH_END_P;
    private final Pattern NOUN_COMPLEX_SUFFIX_WITH_END_P;
    private final Pattern NOUN_COMPLEX_SUFFIX_WITH_SIMPLE_END_P;
    private final Pattern NOUN_SIMPLE_SUFFIX_WITH_END_P;
    private final Pattern ADVERB_SUFFIX_WITH_END_P;

    public RulesStem() {
        ADJECTIVE_COMPLEX_SUFFIX_WITH_END_P = Pattern.compile(ADJECTIVE_AND_PARTICIPLE_COMPLEX_SUFFIX + ADJECTIVE_COMPLEX_END);
        ADJECTIVE_SIMPlE_SUFFIX_WITH_END_P = Pattern.compile(ADJECTIVE_SIMPlE_SUFFIX_WITH_END);

        PARTICIPLE_COMPLEX_SUFFIX_WITH_END_P = Pattern.compile(ADJECTIVE_AND_PARTICIPLE_COMPLEX_SUFFIX + PARTICIPLE_COMPLEX_END);

        VERB_COMPLEX_SUFFIX_WITH_END_P = Pattern.compile(VERB_COMPLEX_SUFFIX + VERB_COMPLEX_END);
        VERB_ONLY_END_P = Pattern.compile(VERB_ONLY_END);
        VERB_COMPLEX_SUFFIX_WITH_SIMPLE_END_P = Pattern.compile(VERB_COMPLEX_SUFFIX + VERB_SIMPLE_END);
        VERB_SIMPLE_SUFFIX_WITH_END_P = Pattern.compile(VERB_SIMPLE_SUFFIX + VERB_COMPLEX_END);

        NOUN_COMPLEX_SUFFIX_WITH_END_P = Pattern.compile(NOUN_COMPLEX_SUFFIX + NOUN_COMPLEX_END);
        NOUN_COMPLEX_SUFFIX_WITH_SIMPLE_END_P = Pattern.compile(NOUN_COMPLEX_SUFFIX + NOUN_SIMPLE_END);
        NOUN_SIMPLE_SUFFIX_WITH_END_P = Pattern.compile(NOUN_SIMPLE_SUFFIX + NOUN_COMPLEX_END);

        ADVERB_SUFFIX_WITH_END_P = Pattern.compile(ADVERB_SUFFIX + ADVERB_END);
    }

    public String stem(String input) {
        String result;

        result = adjectiveDetection(input);
        if(!result.isEmpty()) {
            return result;
        }

        result = participleDetection(input);
        if(!result.isEmpty()) {
            return result;
        }

        result = verbDetection(input);
        if(!result.isEmpty()) {
            return result;
        }

        result = verbDetection(input);
        if(!result.isEmpty()) {
            return result;
        }

        result = adverbDetection(input);
        if(!result.isEmpty()) {
            return result;
        }

        result = nounDetection(input);
        if(!result.isEmpty()) {
            return result;
        }

        return input;
    }

    private String adverbDetection(String input) {
        String result;

        result = find(ADVERB_SUFFIX_WITH_END_P, input);

        if(!result.isEmpty()) {
            return result + "_ADVERB1";
        }

        return "";
    }

    private String nounDetection(String input) {
        String result;

        result = find(NOUN_COMPLEX_SUFFIX_WITH_END_P, input);

        if(!result.isEmpty()) {
            return result + "_NOUN1";
        }

        result = find(NOUN_COMPLEX_SUFFIX_WITH_SIMPLE_END_P, input);

        if(!result.isEmpty()) {
            return result + "_NOUN2";
        }

        result = find(NOUN_SIMPLE_SUFFIX_WITH_END_P, input);

        if(!result.isEmpty()) {
            return result + "_NOUN3";
        }

        return "";
    }


    private String verbDetection(String input) {
        String result;

        result = find(VERB_COMPLEX_SUFFIX_WITH_END_P, input);

        if(!result.isEmpty()) {
            return result + "_VERB1";
        }

        result = find(VERB_ONLY_END_P, input);

        if(!result.isEmpty()) {
            return result + "_VERB2";
        }

        result = find(VERB_COMPLEX_SUFFIX_WITH_SIMPLE_END_P, input);

        if(!result.isEmpty()) {
            return result + "_VERB3";
        }

        result = find(VERB_SIMPLE_SUFFIX_WITH_END_P, input);

        if(!result.isEmpty()) {
            return result + "_VERB4";
        }

        return "";
    }

    private String participleDetection(String input) {
        String result;

        result = find(PARTICIPLE_COMPLEX_SUFFIX_WITH_END_P, input);

        if(!result.isEmpty()) {
            return result + "_PARTICIPLE1";
        }

        return "";
    }

    private String adjectiveDetection(String input) {
        String result;

        if(!input.matches(ADJECTIVE_END_EXCEPTIONS)) {

            result = find(ADJECTIVE_COMPLEX_SUFFIX_WITH_END_P, input);

            if(!result.isEmpty()) {
                return result + "_ADJECTIVE1";
            }

            result = find(ADJECTIVE_SIMPlE_SUFFIX_WITH_END_P, input);

            if(!result.isEmpty()) {
                return result + "_ADJECTIVE2";
            }
        }

        return "";
    }

    private String find(Pattern pattern, String input) {
        Matcher matcher = pattern.matcher(input);
        return matcher.find() ? input.substring(0, matcher.start()) : "";
    }
}
