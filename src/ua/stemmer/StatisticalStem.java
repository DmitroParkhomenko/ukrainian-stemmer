package ua.stemmer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dmitro on 25.02.2015.
 */
public class StatisticalStem implements StemInterface {
    private String [] regexMatrix = {
            "ймося|ємося|емося|имося|імося|" +
                    "алося|илося|ялося|" +
                    "ються|ється|еться|уться|иться|яться|іться|" +
                    "атися|итися|ятися|" +
                    "алися|илися|ялися|" +
                    "йтеся|єтеся|етеся|итеся|" +
                    "уєшся|аєшся|юєшся|" +
                    "мешся|" +
                    "ишся|" +
                    "алася|илася|ялася|" +
                    "вався|ився|явся|" +
                    "уюся|аюся|лююся|люся|" +
                    "уйся|айся|люйся|" +
                    "имуся|жуся|" +
                    "вання|ення|" +
                    "ація|" +
                    "ниця|" +
                    "ля",

            "ймось|ємось|емось|имось|імось|" +
                    "алось|илось|ялось|" +
                    "атись|итись|ятись|" +
                    "ались|ились|ялись|" +
                    "чись|" +
                    "йтесь|єтесь|етесь|итесь|" +
                    "алась|илась|ялась|" +
                    "вавсь|ивсь|явсь|" +
                    "уюсь|аюсь|лююсь|люсь|" +
                    "уйсь|айсь|люйсь|" +
                    "имусь|жусь|" +
                    "вують|тують|кують|" +
                    "кають|" +
                    "люють|" +
                    "іють|" +
                    "яють" +
                    "ність|вість|тість|" +
                    "имуть|нуть|" +
                    "тить|" +
                    "лять|тять|" +
                    "ніть|" +
                    "ать|" +
                    "увань|" +
                    "ень|" +
                    "ець|ниць|" +
                    "ль",

            "аними|еними|ьними|чними|тними|рними|нними|вними|йними|дними|сними|" +
                    "овими|ивими|" +
                    "ькими|" +
                    "стими|" +
                    "лими|" +
                    "ішими|" +
                    "иками|чками|нками|тками|" +
                    "стами|" +
                    "орами|ерами|" +
                    "инами|" +
                    "твами|" +
                    "дами|" +
                    "чами|" +
                    "мами|" +
                    "лами|" +
                    "сами|" +
                    "ннями|" +
                    "стями|" +
                    "ицями|" +
                    "ціями|" +
                    "увати|ювати|" +
                    "кати|" +
                    "ити|" +
                    "іти|" +
                    "нути|" +
                    "яти|" +
                    "сти|" +
                    "ували|ювали|" +
                    "кали|" +
                    "или|" +
                    "нули|" +
                    "іли|" +
                    "яли|" +
                    "ники|" +
                    "чки|нки|тки|" +
                    "уючи|аючи|" +
                    "ини|" +
                    "тори|" +
                    "ери|" +
                    "вши|" +
                    "ди|си|ги",

            "ванім|" +
                    "ленім|ченім|" +
                    "льнім|" +
                    "ичнім|ічнім|" +
                    "тнім|рнім|ннім|внім|ійнім|днім|снім|" +
                    "ковім|ивім|" +
                    "ськім|стім|лім|нішім|" +
                    "ваним|" +
                    "леним|ченим|" +
                    "льним|" +
                    "ичним|ічним|" +
                    "тним|рним|нним|вним|ійним|дним|сним|" +
                    "ковим|ивим|" +
                    "ським|стим|" +
                    "лим|нішим|" +
                    "чим|" +
                    "никам|чкам|нкам|ткам|" +
                    "стам|торам|ерам|" +
                    "инам|твам|" +
                    "дам|чам|мам|лам|сам|" +
                    "ником|" +
                    "стом|" +
                    "тором|ером|" +
                    "оном|ством|змом|сом|дом|лом|" +
                    "анням|енням|остям|ницям|ціям|" +
                    "цем|чем|" +
                    "зм",

            "ному|вому|кому|тому|лому|шому|чому|" +
                    "тиму|" +
                    "зму|" +
                    "вану|лену|чену|льну|ичну|ічну|ину|вну|рну|тну|нну|ійну|дну|ону|сну|" +
                    "ську|нику|чку|нку|тку|" +
                    "кову|ству|ливу|" +
                    "сту|ату|" +
                    "тору|еру|" +
                    "ачу|лу|нішу|" +
                    "ду|жу|су|гу|зу|щу",

            "уймо|аймо|юймо|іймо|яймо|" +
                    "уємо|аємо|юємо|іємо|яємо|" +
                    "мемо|немо|" +
                    "тимо|" +
                    "німо|" +
                    "ного|вого|кого|того|лого|шого|чого|" +
                    "вало|кало|ило|нуло|іло|яло|" +
                    "вано|ено|" +
                    "ство|" +
                    "ко|то",

            "анні|енні|інні|" +
                    "вані|лені|чені|льні|ичні|ічні|вні|ині|тні|рні|ійні|дні|оні|сні|" +
                    "кові|тові|рові|нові|мові|дові|сові|лові|" +
                    "цеві|чеві|" +
                    "стві|" +
                    "ливі|" +
                    "єві|" +
                    "ості|аті|" +
                    "ниці|нці|чці|вці|" +
                    "ські|" +
                    "торі|ері|" +
                    "лі|ачі|змі|ді|сі|ніші|зі",

            "аною|еною|ьною|чною|вною|тною|рною|нною|йною|иною|дною|сною|" +
                    "ькою|нкою|чкою|ткою|" +
                    "овою|ивою|" +
                    "стою|лою|ішою|рою|" +
                    "анню|енню|інню|" +
                    "істю|" +
                    "овую|тую|кую|" +
                    "ацію|" +
                    "каю|" +
                    "цією|" +
                    "ницю|" +
                    "ицею|" +
                    "лю|" +
                    "люю|яю",

            "аній|еній|ьній|чній|тній|" +
                    "рній|нній|вній|йній|дній|сній|" +
                    "овій|ивій|" +
                    "ькій|стій|лій|ішій|" +
                    "аний|ений|ьний|чний|тний|рний|нний|вний|йний|дний|сний|" +
                    "овий|ивий|" +
                    "ький|стий|іший|чий|" +
                    "овуй|туй|куй|" +
                    "кай|вай|" +
                    "стей|люй|яй",

            "вана|лена|чена|льна|ична|ічна|вна|тна|рна|нна|ина|ійна|дна|сна|" +
                    "вала|кала|ила|нула|іла|яла|" +
                    "ська|ника|чка|нка|тка|" +
                    "чка|нка|тка|" +
                    "кова|ства|лива|" +
                    "ста|ата|" +
                    "тора|ера|" +
                    "ача|ніша|" +
                    "да|ма|га",

            "уйте|айте|юйте|ійте|яйте|" +
                    "уєте|аєте|юєте|ієте|яєте|" +
                    "мете|нете|" +
                    "тите|сте|" +
                    "ване|лене|чене|льне|ичне|ічне|тне|рне|нне|вне|ійне|дне|сне|" +
                    "кове|ливе|" +
                    "ське|тиме|ле|ніше|че",

            "аних|ених|ьних|чних|тних|рних|нних|вних|ійних|дних|сних|" +
                    "ових|ивих|" +
                    "ьких|" +
                    "стих|" +
                    "лих|іших|" +
                    "чих|" +
                    "иках|нках|чках|тках|" +
                    "стах|орах|ерах|инах|твах|дах|чах|мах|сах|лах|" +
                    "ннях|стях|ицях|ціях",

            "ної|вої|кої|тої|лої|шої|" +
                    "ції",

            "ків|тів|рів|нів|ців|лів|чів|дів|сів|мів|" +
                    "вав|кав|" +
                    "ив|нув|яв|тв",

            "уєш|аєш|юєш|ієш|яєш|" +
                    "имеш|неш|тиш",

            "вує|тує|кує|кає|лює|іє|яє",
            "чок|нок|ток|ник",
            "ст",
            "тор|ер",
            "ин|он",
            "ач"
    };

    private String[] transformedRegexMatrix;

    public StatisticalStem() {
        final int MIN_END_LENGTH = 2;
        transformedRegexMatrix = new String[regexMatrix.length];

        for (int i = 0; i < regexMatrix.length; i++) {
            String[] parts = regexMatrix[i].split("\\|");
            String currPattern = "";

            for (String part : parts) {
                String transformedPart = "";

                for (int j = 0; j < part.length(); j++) {
                    if (j < part.length() - MIN_END_LENGTH) {
                        transformedPart += part.charAt(j) + "?";
                    } else {
                        transformedPart += part.charAt(j);
                    }
                }

                currPattern += transformedPart + "|";
            }

            transformedRegexMatrix[i] = currPattern.substring(0, currPattern.length() - 1);
        }
    }

    public String stem(String input) {
        input = input.toLowerCase();
        char lastChar = input.charAt(input.length() - 1);
        Pattern pattern = null;

        switch (lastChar) {
            case 'я': pattern = Pattern.compile(transformedRegexMatrix[0]);
                break;
            case 'ь': pattern = Pattern.compile(transformedRegexMatrix[1]);
                break;
            case 'и': pattern = Pattern.compile(transformedRegexMatrix[2]);
                break;
            case 'м': pattern = Pattern.compile(transformedRegexMatrix[3]);
                break;
            case 'у': pattern = Pattern.compile(transformedRegexMatrix[4]);
                break;
            case 'о': pattern = Pattern.compile(transformedRegexMatrix[5]);
                break;
            case 'і': pattern = Pattern.compile(transformedRegexMatrix[6]);
                break;
            case 'ю': pattern = Pattern.compile(transformedRegexMatrix[7]);
                break;
            case 'й': pattern = Pattern.compile(transformedRegexMatrix[8]);
                break;
            case 'а': pattern = Pattern.compile(transformedRegexMatrix[9]);
                break;
            case 'е': pattern = Pattern.compile(transformedRegexMatrix[10]);
                break;
            case 'х': pattern = Pattern.compile(transformedRegexMatrix[11]);
                break;
            case 'ї': pattern = Pattern.compile(transformedRegexMatrix[12]);
                break;
            case 'в': pattern = Pattern.compile(transformedRegexMatrix[13]);
                break;
            case 'ш': pattern = Pattern.compile(transformedRegexMatrix[14]);
                break;
            case 'є': pattern = Pattern.compile(transformedRegexMatrix[15]);
                break;
            case 'к': pattern = Pattern.compile(transformedRegexMatrix[16]);
                break;
            case 'т': pattern = Pattern.compile(transformedRegexMatrix[17]);
                break;
            case 'р': pattern = Pattern.compile(transformedRegexMatrix[18]);
                break;
            case 'н': pattern = Pattern.compile(transformedRegexMatrix[19]);
                break;
            case 'ч': pattern = Pattern.compile(transformedRegexMatrix[20]);
                break;
        }

        if(pattern == null) {
            return input;
        }

        String result = find(pattern, input);
        if(!result.isEmpty()) {
            return result + "_STATISTICAL";
        }

        return input;
    }

    private String find(Pattern pattern, String input) {
        Matcher matcher = pattern.matcher(input);
        return matcher.find() ? input.substring(0, matcher.start()) : "";
    }
}