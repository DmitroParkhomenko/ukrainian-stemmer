package ua.stemmer;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmitro on 25.02.2015.
 */
public class UkrainianStem {
    private final int MIN_WORD_LEN = 4;
    private final String WORDS_IN_TEXT_SPLITTER_REGEX = "[^\\p{L}'\\0027']+"; // p here is unicode character class
    private String path;

    public static void main(String[] args) throws IOException {
//        String path = "Na-Zahidnomu-Fronti-bez-zmin-Erih-Mariya-Remark.txt";
//        String path = "testCase2.txt";
        String path = "taekwondo.txt";

        UkrainianStem ukrainianStem = new UkrainianStem(path);
        List<String> stringList = ukrainianStem.run();
        for (int i = 0; i < stringList.size(); i++) {
            String s = stringList.get(i);
            System.out.println(s);
        }
    }

    public UkrainianStem(String path) {
        this.path = path;
    }

    public List<String> run() throws IOException  {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = null;

        try {
            StemInterface rulesStemmer = new RulesStem();
            StemInterface statisticalStemmer = new StatisticalStem();
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] words = line.split(WORDS_IN_TEXT_SPLITTER_REGEX);
                for (String item : words) {
                    String token = item.toLowerCase();
                    if (constrains(token)) {
                        String s1 = rulesStemmer.stem(token);
                        String s2 = statisticalStemmer.stem(token);
                        result.add(s1.length() <= s2.length() ? s1 : s2);
                    }
                }
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
        }

        return result;
    }

    private boolean constrains(String item) {
        return item.length() >= MIN_WORD_LEN;
    }
}